# GUACAMOLE DEMO
---

Run:
`ansible-playbook playbook.yml`

Use:
point your browser to https://guacamole.lvh.me/guacamole and login with
```
username: guacadmin
password: guacadmin
```
For admin interface

LDAP users can be found here https://github.com/rroemhild/docker-test-openldap

You can add and existing ldap user without a password and then assign them connections

:D
